# Imports
import glob
import os
from PIL import Image, ImageOps
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

# global variables
counter = 0
desired_size = 128
imageMatrix = []
outputMatrix = []
labelMatrix = []
failures = []
source_images_dir = 'gear_images'
target_images_dir = 'gear_images_output_with_labels'

_labels = {
    1: "axes",
    2: "carabiners",
    3: "gloves",
    4: "harnesses",
    5: "insulated_jackets",
    6: "rope",
    7: "boots",
    8: "crampons",
    9: "hardshell_jackets",
    10: "helmets",
    11: "pulleys",
    12: "tents"}

# utilities


def processImage(imagePath, label):
    if imagePath.endswith(('.jpg', '.jpeg', '.png')):
        image = Image.open(imagePath)
        w, h = image.size
        if(w == h & w > desired_size):
            return imageResizeToDesired(image)
        else:
            return processNonSquareImagesWithPill(imagePath)
    else:
        return None


def normalize(arr):
    """
    Linear normalization
    http://en.wikipedia.org/wiki/Normalization_%28image_processing%29
    """

    arr = arr.astype('float')
    # Do not touch the alpha channel
    for i in range(3):
        arr[..., i] = arr[..., i]/255
        """
        minval = arr[...,i].min()
        maxval = arr[...,i].max()
        if minval != maxval:
            arr[...,i] -= minval
            arr[...,i] *= (255.0/(maxval-minval))
        """
    return arr


def imageResizeToDesired(image_var):   
    newimage = image_var.resize((desired_size, desired_size), Image.ANTIALIAS)
    whiteImage = Image.new(
        "RGB", (desired_size, desired_size), (255, 255, 255))
    whiteImage.paste(newimage, (0, 0))    
    myarray = normalize(np.asarray(whiteImage))    
    return myarray


def processDir(dirName, labelKey):
    global imageMatrix, counter
    for filepath in glob.iglob(source_images_dir+'/'+dirName+'/*'):
        myarray = processImage(filepath, dirName)
        if(myarray is not None and myarray.shape == (128, 128, 3)):
            outputMatrix.append(myarray)
            imageMatrix.append(myarray/255)
            labelMatrix.append(dirName)
            counter = counter + 1
        else:
            failures.append(filepath)


def processNonSquareImagesWithPill(imagePath):
    im = Image.open(imagePath)
    desired_size = max(im.size)
    old_size = im.size  # old_size[0] is in (width, height) format
    ratio = float(desired_size)/max(old_size)
    new_size = tuple([int(x*ratio) for x in old_size])
    im = im.resize(new_size, Image.ANTIALIAS)
    new_im = Image.new("RGB", (desired_size, desired_size), 'WHITE')
    new_im.paste(im, ((desired_size-new_size[0])//2,
                      (desired_size-new_size[1])//2))
    myarray = imageResizeToDesired(new_im)                      
    return myarray

def loopThroughLabels():
    if not os.path.exists(target_images_dir):
        os.makedirs(target_images_dir)
    for key, value in _labels.items():
        processDir(value, key)


# execute
# sourceimagepath = 'gear_images/hardshell_jackets/111875.jpeg'
# x=processNonSquareImagesWithPill(sourceimagepath)
# print(x.shape)
loopThroughLabels()
imageMatrixNP = np.array(imageMatrix)
labelMatrixNP = np.array(labelMatrix)

print(labelMatrixNP.shape)
