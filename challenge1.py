# Expected Output
# b'{"id":"6e44fd9d-ae0a-4999-bc78-a319d0f6ae1c","project":"60abd36d-aeb0-4425-b0d2-33f8215f1007","iteration":"227a5457-533a-4ddf-b132-e3a969ba8b3f","created":"2018-11-05T11:12:54.1849066Z","predictions":[{"probability":0.958659947,"tagId":"5acfaac5-a441-42be-9b45-8dc49767e210","tagName":"hardshell_jackets"},{"probability":0.0467824452,"tagId":"5e3d51aa-edc5-49f4-abab-29aa8c5db151","tagName":"insulated_jackets"}]}'

import http.client, urllib.request, urllib.parse, urllib.error, base64
# from PIL import Image
# image = Image.open('/data/home/admin-openhack-table4/notebooks/Table4/Sid/Challenge1/gear_images/hardshell_jackets/10116634x1038116_zm.jpeg')
# imageinbytes = base64.b64encode(image.tobytes())
image = open('/data/home/admin-openhack-table4/notebooks/Table4/Sid/Challenge1/gear_images/hardshell_jackets/10116634x1038116_zm.jpeg', 'rb') 
#open binary file in read mode 
image_read = image.read()
image_64_encode = base64.encodebytes(image_read)
# x=image_64_encode.toString();

headers = {
    # Request headers
    'Prediction-Key': '',
    'Content-Type': 'multipart/form-data',
    'Prediction-key': '0c272173ccf545498460a5963e104b32',
}

params = urllib.parse.urlencode({
    # Request parameters
    'iterationId': 'Iteration 1',
#     'application': '{string}',
})

try:
    conn = http.client.HTTPSConnection('southcentralus.api.cognitive.microsoft.com')
    conn.request("POST", "/customvision/v2.0/Prediction/60abd36d-aeb0-4425-b0d2-33f8215f1007/image", image_read, headers)
    response = conn.getresponse()
    data = response.read()
    print(data)
    conn.close()
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))

