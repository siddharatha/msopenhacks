# Use Case
AdventureWorks, a major outdoor and climbing gear retailer, wants to understand customer behavior by learning more about the gear that consumers wear and will accomplish this with powerful Microsoft and open-source technologies for Computer Vision.

## Credentials 

### azure portal & customvision.ai
- username : hacker1@otaprd389ops.onmicrosoft.com
- password: vavCk@0ycJ34

### jupyter
- username : admin-openhack-table4
- password: p!&V!sRETuH$RB
- 

# Team
- Sid
- Martin (data scientist)
- Thomas (project manager)
- Charles (Developer/project manager)
- Elif (Alternant developer)
  
# DSVM - Challenge 0
- Create DSVM directly from azure portal
- Use the Ubuntu DSVM (CSP is not available)
- NC6 machine with GPU .

## Gear Images Download in Jupyter notebook
``` sh
curl -O https://challenge.blob.core.windows.net/challengefiles/gear_images.zip
```


# Train and Run Predictions with CustomVision.ai - Challenge1

## References
- (Demystifying AI)[https://www.youtube.com/watch?v=k-K3g4FKS_c&feature=youtu.be]

- Url for project: https://customvision.ai/projects/60abd36d-aeb0-4425-b0d2-33f8215f1007#/performance
  ![PredictionURL][predictionurl.png]  
- http post requests to upload the images to customvision.ai
- Use the 2 classifications for jackets dataset
- Train the images with the 2 classifiers.
- Run the challenge1.py to validate the response of the image.

## Steps
### Method 1
- Check if the Image is a square
    - If its a square and bigger than desired size -> resize to desired size and equalize the image.
    - If its smaller than desired size or not a square
        - Create a white image with the maximum of height/width from the image. (use pillow)
        - Resize the image , equalize it if its a png , and paste it on top of the white image.
        - Convert the array to numpy array
        - Normalize the array
    - maintain in a global variable, you can use jupyter notebooks to process them.

### Method 2
- Pick a square.


